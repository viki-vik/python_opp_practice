
class Human:
  def __init__(self, fname, lname):
    self.firstname = fname
    self.lastname = lname

  def printname(self):
    print(self.firstname, self.lastname)

person = Human("John", "Doe")
person.printname()

class Animal(Human):
    def __init__(self, name, owner_fname, owner_lname, age):
        self.name = name
        super().__init__(owner_fname, owner_lname)
        self.age = age

    def animal_details(self):
        print("The owner of ", self.name, " is ", self.firstname, self.lastname, "\nThe age of the pet is ", self.age)

pet = Animal("Spark","Venus","Williams",2016)
pet.animal_details()

pet = Animal("Spark",person.firstname, person.lastname ,2016)
pet.animal_details()

class Robot(Human):
    def __init__(self, model, brand, client_fname, client_lname, price):
        self.model = model
        self.brand = brand
        super().__init__(client_fname, client_lname)
        self.price = price
    def update_owner(self, client_name):
        client = client_name.split()
        self.firstname = client[0]
        self.lastname = client[1]
    def update_price(self, price):
        self.price = price

robot = Robot("S6", "Roborock", "Serena", "Williams", 800)
robot.printname()

robot.update_owner("Maria Sharapova")
robot.printname()